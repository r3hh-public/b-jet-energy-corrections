/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author TJ Khoo


#ifndef BJetCalibrationSystAlg_H
#define BJetCalibrationSystAlg_H

#include "AnaAlgorithm/AnaAlgorithm.h"
#include <SystematicsHandles/SysCopyHandle.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "BJetCalibrationTool/IBJetCalibrationTool.h"

/// \brief an algorithm for calling \ref IBJetCalibrationTool

class BJetCalibrationSystAlg final : public EL::AnaAlgorithm
{
 public:
  /// \brief the standard constructor
  BJetCalibrationSystAlg (const std::string& name, 
			  ISvcLocator* pSvcLocator);

  StatusCode initialize () override;

  StatusCode execute () override;

 private:
  /// \brief the calibration tool
  ToolHandle<IBJetCalibrationTool> m_calibrationTool{
    this, "BJetCalibTool", "", "Tool implementing the calibration"};

  /// \brief configuration for retrieval, copy and output of jets
  CP::SysListHandle m_systematicsList {this};
  CP::SysCopyHandle<xAOD::JetContainer> m_jetHandle {
    this, "jets", "AnalysisJets", "the jet collection to run on"};

  CP::SysReadHandle<xAOD::MuonContainer> m_muonHandle{
    this, "muons", "AnalysisMuons", "Muon collection to retrieve"};

  Gaudi::Property<float> m_minPt{
    this, "minPt_bJets", 20e3, "Minimum pT of b-jets to be corrected"};

  Gaudi::Property<float> m_maxEta{
    this, "maxEta_bJets", 2.5, "Maximum eta of b-jets to be corrected"};

  /// \brief decoration name for b-jet preselection
  Gaudi::Property<std::string> m_btagSelDecor {
    this, "btagSelDecor", "", "decoration holding b-jet pass info"};
  SG::AuxElement::ConstAccessor<char> m_acc_btagSel{"btagSelAcc"};

};
#endif

