from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AnalysisAlgorithmsConfig.ConfigBlock import ConfigBlock
from AthenaConfiguration.Enums import LHCPeriod


class BJetPtCalibrationConfig(ConfigBlock):
    """
    Configuration for applying pt corrections to b-jets
    """

    def __init__(self, containerName):
        super(BJetPtCalibrationConfig, self).__init__()
        self.containerName = containerName
        self.addOption('muonQuality', 1, type=int)
        self.addOption('muonName', '', type=str)
        self.addOption('btagSelDecor', '', type=str)
        self.addOption('doPtCorr', True, type=bool)

    def makeAlgs(self, config):

        alg = config.createAlgorithm(
            'BJetCalibrationSystAlg',
            'BJetCalibAlg_'+self.containerName,
        )
        alg.muons = self.muonName
        alg.btagSelDecor = self.btagSelDecor
        alg.jets = config.readName(self.containerName)
        alg.jetsOut = config.copyName(self.containerName)

        config.addPrivateTool(
            'BJetCalibTool',
            'BJetCalibrationTool',
        )
        # JetCollection is only used for the PtReco, i.e. only in the case of small-R jets
        alg.BJetCalibTool.JetCollection = 'AntiKt4EMPFlow'
        alg.BJetCalibTool.doPtReco = self.doPtCorr

        config.addPrivateTool('BJetCalibTool.MuonSelectionTool', 'CP::MuonSelectionTool')
        alg.BJetCalibTool.MuonSelectionTool.MaxEta = 2.5
        alg.BJetCalibTool.MuonSelectionTool.MuQuality = self.muonQuality
        alg.BJetCalibTool.MuonSelectionTool.IsRun3Geo = config.geometry() >= LHCPeriod.Run3

        if("AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets" in alg.jets):
            alg.BJetCalibTool.doUFO = True


def makeBJetPtCalibrationConfig(
    seq,
    containerName,
):
    config = BJetPtCalibrationConfig(containerName)
    seq.append(config)


def BJetCalibToolCfg(
    flags,
    # Medium, see Event/xAOD/xAODMuon/xAODMuon/versions/Muon_v1.h
    # in atlas/athena
    muonQuality=1,
    jetName='AntiKt4EMPFlow',
    **kwargs
):

    cfg = ComponentAccumulator()
    from MuonSelectorTools.MuonSelectorToolsConfig import MuonSelectionToolCfg
    mst = cfg.popToolsAndMerge(MuonSelectionToolCfg(flags, MaxEta=2.5, MuQuality=muonQuality))

    bjc = CompFactory.BJetCalibrationTool(
        "BJetCalibrationTool",
        JetCollection=jetName,
        MuonSelectionTool=mst,
        **kwargs
    )

    cfg.setPrivateTools(
        [mst, bjc]
    )
    return cfg


def BJetCalibSystAlgCfg(
    flags,
    jetName,
    muonName,
    # Name of decoration marking selected b-jets
    btagSelDecor,
    muonQuality=1,
    calibSuffix='bjetcalib',
    **kwargs
):

    cfg = ComponentAccumulator()
    # We don't pass the jet collection name here, because the tool
    # just wants the appropriate one for the derived corrections.
    # Assume we always use PFlow, which is the only one recommended
    toolcfg = BJetCalibToolCfg(flags, muonQuality, **kwargs)
    tools = cfg.popToolsAndMerge(toolcfg)
    bjc = tools[-1]
    alg = CompFactory.BJetCalibrationSystAlg(
        'BJetCalibAlg',
        jets=jetName,
        jetsOut=f'{jetName}_{calibSuffix}',
        muons=muonName,
        btagSelDecor=btagSelDecor,
        BJetCalibTool=bjc,
    )
    if alg.jets=="AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets":
        alg.BJetCalibTool.doUFO = True
        alg.BJetCalibTool.doPtReco = False
    cfg.addEventAlgo(alg)

    return cfg
